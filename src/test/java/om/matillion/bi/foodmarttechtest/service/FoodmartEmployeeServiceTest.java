package om.matillion.bi.foodmarttechtest.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import om.matillion.bi.foodmarttechtest.dao.MySqlEmployeeRepository;
import om.matillion.bi.foodmarttechtest.dto.EmployeeRepresentation;
import om.matillion.bi.foodmarttechtest.dto.Result;
import om.matillion.bi.foodmarttechtest.model.Employee;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FoodmartEmployeeServiceTest
{
    @Mock
    private MySqlEmployeeRepository mockEmployeeRepo;

    @InjectMocks
    private FoodmartEmployeeService employeeService;

    @Test
    public void findEmployeesByDepartmentPaytypeAndEducationLevel_repoErrorResult_errorResult() throws SQLException
    {
        Result<List<Employee>> repoCallResult = Result.error("test");

        // given
        given(mockEmployeeRepo.findEmployeesByDepartmentPaytypeAndEducationLevel(anyString(), anyString(),
                anyString())).willReturn(repoCallResult);

        // when
        Result<List<EmployeeRepresentation>> result = employeeService
                .findEmployeesByDepartmentPaytypeAndEducationLevel("", "", "");

        // then
        assertNotNull(result);
        assertFalse(result.isSuccess());
        assertNotNull(result.getErrorMessage());
        assertEquals(repoCallResult.getErrorMessage(), result.getErrorMessage());
    }

    @Test
    public void findEmployeesByDepartmentPaytypeAndEducationLevel_repoSuccesfulResult_succesfulResult()
            throws SQLException
    {
        // given
        given(mockEmployeeRepo.findEmployeesByDepartmentPaytypeAndEducationLevel(anyString(), anyString(),
                anyString())).willReturn(Result.success(new ArrayList<>()));

        // when
        Result<List<EmployeeRepresentation>> result = employeeService
                .findEmployeesByDepartmentPaytypeAndEducationLevel("", "", "");

        // then
        assertNotNull(result);
        assertTrue(result.isSuccess());
        assertNotNull(result.getResult());
    }
}
