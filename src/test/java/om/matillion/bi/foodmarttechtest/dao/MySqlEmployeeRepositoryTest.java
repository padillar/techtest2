package om.matillion.bi.foodmarttechtest.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.willThrow;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;

import om.matillion.bi.foodmarttechtest.dto.Result;
import om.matillion.bi.foodmarttechtest.model.Employee;

@RunWith(MockitoJUnitRunner.class)
public class MySqlEmployeeRepositoryTest 
{
    @Mock
    private ConnectionRetriever mockConnectionRetriever;
    
    @InjectMocks
    MySqlEmployeeRepository employeeRepo;
    
    @Test
    public void findEmployeesByDepartmentPaytypeAndEducationLevel_invalidConnection_errorResult() throws SQLException
    {        
        // given
        willThrow(new SQLException()).given(mockConnectionRetriever).getConnection();
        
        // when
        Result<List<Employee>> result = employeeRepo.findEmployeesByDepartmentPaytypeAndEducationLevel("", "", "");
        
        // then
        assertNotNull(result);
        assertFalse(result.isSuccess());
        assertNotNull(result.getErrorMessage());
    }
    
    @Test
    public void findEmployeesByDepartmentPaytypeAndEducationLevel_validConnection_succesfulResult() throws SQLException
    {
        Connection mockConnection = mock(Connection.class);
        PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
        ResultSet mockResultSet = mock(ResultSet.class);
        
        when(mockConnectionRetriever.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(false);
        
        Result<List<Employee>> result = employeeRepo.findEmployeesByDepartmentPaytypeAndEducationLevel("", "", "");
        
        assertNotNull(result);
        assertTrue(result.isSuccess());
        assertNotNull(result.getResult());
    }
}
