package om.matillion.bi.foodmarttechtest.model;

import java.util.Objects;

/**
 * Model object to represent an Employee.
 */
public class Employee
{
    private static final String TO_STRING_TEMPLATE = "First Name: %s, Last Name: %s, Education Level: %s, Salary: %s";

    private final String firstName;
    private final String lastName;
    private final String educationLevel;
    private final int salary;

    public Employee(String firstName, String lastName, String educationLevel, int salary)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.educationLevel = educationLevel;
        this.salary = salary;
    }

    @Override
    public String toString()
    {
        return String.format(TO_STRING_TEMPLATE, firstName, lastName, educationLevel, salary);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!(obj instanceof Employee)) return false;
        Employee o = (Employee) obj;
        return Objects.equals(firstName, o.firstName) && Objects.equals(lastName, o.lastName)
                && Objects.equals(educationLevel, o.educationLevel) && Objects.equals(salary, o.salary);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(firstName, lastName, educationLevel, salary);
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }
}
