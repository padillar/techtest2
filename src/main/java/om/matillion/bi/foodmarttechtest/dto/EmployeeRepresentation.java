package om.matillion.bi.foodmarttechtest.dto;

import java.util.Objects;

/**
 * This DTO contains the data about an Employee that we want the front end to be
 * able to see.
 */
public class EmployeeRepresentation
{
    private final String firstName;
    private final String lastName;

    public EmployeeRepresentation(String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString()
    {
        return String.format("%s %s", firstName, lastName);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!(obj instanceof EmployeeRepresentation)) return false;
        EmployeeRepresentation o = (EmployeeRepresentation) obj;
        return Objects.equals(firstName, o.firstName) && Objects.equals(lastName, o.lastName);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(firstName, lastName);
    }
}
