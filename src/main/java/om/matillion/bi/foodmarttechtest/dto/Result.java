package om.matillion.bi.foodmarttechtest.dto;

/**
 * Generic result object which will hold your result if it was successful, an
 * error message if it was not and an isSuccess boolean so you can call to tell.
 * 
 * A typical use case might be in a command where you want to get something from
 * the server. The server can return this to the command and the command can
 * handle it from there. Look at GetMetadataSample for an example.
 * 
 * @param <T>
 */
public class Result<T>
{
    private final T result;
    private final String errorMessage;
    private final boolean isSuccess;

    /**
     * Local factory method which creates a successful result.
     */
    public static <T> Result<T> success(T result)
    {
        return new Result<T>(result, null, true);
    }

    /**
     * Local factory method which creates an errored result.
     */
    public static <T> Result<T> error(String errorMessage)
    {
        return new Result<T>(null, errorMessage, false);
    }

    public Result(T result, String errorMessage, boolean isSuccess)
    {
        this.result = result;
        this.errorMessage = errorMessage;
        this.isSuccess = isSuccess;
    }

    /**
     * @return null if it errored or the result if successful
     */
    public T getResult()
    {
        return result;
    }

    /**
     * @return error message if it errored or null if successful
     */
    public String getErrorMessage()
    {
        return errorMessage;
    }

    public boolean isSuccess()
    {
        return isSuccess;
    }
}
