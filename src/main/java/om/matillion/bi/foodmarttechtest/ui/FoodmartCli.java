package om.matillion.bi.foodmarttechtest.ui;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import om.matillion.bi.foodmarttechtest.dto.EmployeeRepresentation;
import om.matillion.bi.foodmarttechtest.dto.Result;
import om.matillion.bi.foodmarttechtest.service.EmployeeService;

/**
 * FoodmartCli represents a UI from the command line for the FoodmartApp.
 */
public class FoodmartCli
{
    private static final String SUCCESSFUL_FIND_MESSAGE = "For department %s, pay type %s, education %s, the following employees where found: %s";

    private final EmployeeService employeeService;

    public FoodmartCli(EmployeeService employeeService)
    {
        this.employeeService = employeeService;
    }

    /**
     * Runs the CLI to prompt the user for input and give feedback.
     * 
     * @return the search criteria the user has input
     */
    public void startCommandLineApp()
    {
        String department;
        String payType;
        String education;
        try (Scanner scanner = new Scanner(System.in))
        {
            System.out.print("Enter employee deparment: ");
            department = scanner.nextLine();
            System.out.print("Enter employee pay type: ");
            payType = scanner.nextLine();
            System.out.print("Enter employee education level: ");
            education = scanner.nextLine();
        }

        Result<List<EmployeeRepresentation>> result = employeeService
                .findEmployeesByDepartmentPaytypeAndEducationLevel(department, payType, education);
        displayResult(result, department, payType, education);
    }

    private void displayResult(Result<List<EmployeeRepresentation>> result, String department, String payType,
            String education)
    {
        if (!result.isSuccess())
        {
            System.out.println(result.getErrorMessage());
            return;
        }

        String employeeString = result.getResult().stream().map(EmployeeRepresentation::toString)
                .collect(Collectors.joining(", "));
        System.out.println(String.format(SUCCESSFUL_FIND_MESSAGE, department, payType, education, employeeString));
    }
}
