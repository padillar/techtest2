package om.matillion.bi.foodmarttechtest;

import om.matillion.bi.foodmarttechtest.dao.ConnectionRetriever;
import om.matillion.bi.foodmarttechtest.dao.MySqlEmployeeRepository;
import om.matillion.bi.foodmarttechtest.service.EmployeeService;
import om.matillion.bi.foodmarttechtest.service.FoodmartEmployeeService;
import om.matillion.bi.foodmarttechtest.ui.FoodmartCli;

/**
 *  FoodmartApplication is the entry point into this program.
 */
public class FoodmartApplication 
{
    public static void main( String[] args )
    {
        EmployeeService employeeService = new FoodmartEmployeeService(new MySqlEmployeeRepository(new ConnectionRetriever()));
        FoodmartCli foodmartCli = new FoodmartCli(employeeService);
        foodmartCli.startCommandLineApp();
    }
}
