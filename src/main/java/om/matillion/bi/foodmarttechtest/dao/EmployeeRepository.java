package om.matillion.bi.foodmarttechtest.dao;

import java.util.List;

import om.matillion.bi.foodmarttechtest.dto.Result;
import om.matillion.bi.foodmarttechtest.model.Employee;

/**
 * An EmployeeDao represents a store of employees.
 */
public interface EmployeeRepository
{
    /**
     * @param department
     *            - Part of the company the employee works
     * @param payType
     *            - How the employee is paid e.g. Monthly
     * @param education
     *            - The education level of the employee
     * 
     * @return A ResultObject containing the employees
     */
    Result<List<Employee>> findEmployeesByDepartmentPaytypeAndEducationLevel(String department, String payType,
            String education);
}
