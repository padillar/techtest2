package om.matillion.bi.foodmarttechtest.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Handles getting a connection to the database.
 */
public class ConnectionRetriever
{
    private static final String JDBC_URL = "mysql-technical-test.cq5i4y35n9gg.eu-west-1.rds.amazonaws.com";
    private static final String JDBC_USERNAME = "technical_test";
    private static final String JDBC_PASSWORD = "HopefullyProspectiveDevsDontBreakMe";

    /**
     * @return an open connection that the client must close.
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException
    {
        return DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
    }
}
