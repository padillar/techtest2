package om.matillion.bi.foodmarttechtest.dao;

import om.matillion.bi.foodmarttechtest.dto.Result;
import om.matillion.bi.foodmarttechtest.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlEmployeeRepository implements EmployeeRepository
{
    private static final String GET_EMPLOYEES_QUERY = "SELECT employee.first_name, employee.last_name FROM employee "
            + "JOIN department USING (department_id) " + "JOIN position USING (position_id) "
            + "WHERE department_description=? " + "AND pay_type=? " + "AND education_level=?";

    private static final Logger LOGGER = LoggerFactory.getLogger(MySqlEmployeeRepository.class);

    private final ConnectionRetriever connectionRetriever;

    public MySqlEmployeeRepository(ConnectionRetriever connectionRetriever)
    {
        this.connectionRetriever = connectionRetriever;
    }

    @Override
    public Result<List<Employee>> findEmployeesByDepartmentPaytypeAndEducationLevel(String department,
            String payType, String education)
    {
        List<Employee> employees = new ArrayList<>();
        try (Connection connection = connectionRetriever.getConnection();
                PreparedStatement statement = connection.prepareStatement(GET_EMPLOYEES_QUERY))
        {
            statement.setString(1, department);
            statement.setString(2, payType);
            statement.setString(3, education);
            try (ResultSet results = statement.executeQuery())
            {
                while (results.next())
                {
                    String firstName = results.getString("first_name");
                    String lastName = results.getString("last_name");
                    String educationLevel = results.getString("education_level");
                    int salary = results.getInt("salary");

                    employees.add(new Employee(firstName, lastName, educationLevel, salary));
                }
            }
        }
        catch (SQLException e)
        {
            String errorMessage = "An error occured while querying the database: " + e.getMessage();
            LOGGER.error(errorMessage, e);
            return Result.error(errorMessage);
        }

        return Result.success(employees);
    }
}
