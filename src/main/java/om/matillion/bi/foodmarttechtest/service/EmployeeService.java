package om.matillion.bi.foodmarttechtest.service;

import om.matillion.bi.foodmarttechtest.dto.EmployeeRepresentation;
import om.matillion.bi.foodmarttechtest.dto.Result;

import java.util.List;

/**
 * An EmployeeService provides provides a way to apply logic to Employee models
 */
public interface EmployeeService
{
    /**
     * @param department
     *            - Part of the company the employee works
     * @param payType
     *            - How the employee is paid e.g. Monthly
     * @param education
     *            - The education level of the employee
     *
     * @return a list of representations of employees wrapped in a result object
     *         which may contain an error message.
     */
    Result<List<EmployeeRepresentation>> findEmployeesByDepartmentPaytypeAndEducationLevel(String department,
            String payType, String education);
}
