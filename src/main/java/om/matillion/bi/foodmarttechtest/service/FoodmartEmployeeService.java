package om.matillion.bi.foodmarttechtest.service;

import java.util.List;
import java.util.stream.Collectors;

import om.matillion.bi.foodmarttechtest.dao.EmployeeRepository;
import om.matillion.bi.foodmarttechtest.dto.EmployeeRepresentation;
import om.matillion.bi.foodmarttechtest.dto.Result;
import om.matillion.bi.foodmarttechtest.model.Employee;

public class FoodmartEmployeeService implements EmployeeService
{
    private final EmployeeRepository employeeRepo;

    public FoodmartEmployeeService(EmployeeRepository employeeRepo)
    {
        this.employeeRepo = employeeRepo;
    }

    @Override
    public Result<List<EmployeeRepresentation>> findEmployeesByDepartmentPaytypeAndEducationLevel(String department,
            String payType, String education)
    {
        Result<List<Employee>> findEmployeesResult = employeeRepo
                .findEmployeesByDepartmentPaytypeAndEducationLevel(department, payType, education);
        if (!findEmployeesResult.isSuccess()) 
            return Result.error(findEmployeesResult.getErrorMessage());

        List<EmployeeRepresentation> employeeRepresentations = findEmployeesResult.getResult().stream()
                .map(e -> new EmployeeRepresentation(e.getFirstName(), e.getLastName()))
                .collect(Collectors.toList());
        return Result.success(employeeRepresentations);
    }
}
